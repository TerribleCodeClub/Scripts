#!/bin/bash -e

RESOLUTION_X=1920
RESOLUTION_Y=1080

VIDEO_CODEC="libx264"
VIDEO_BITRATE="2M"

AUDIO_CODEC="aac"
AUDIO_BITRATE="192K"

function cleanup {
    rm -rf ./.tmp/
    rm -rf ./.list.txt
    rm -rf ./.shuffled.txt
}

trap cleanup EXIT


mkdir -p ./.tmp/
mkdir -p ./out/


shopt -s nullglob
shopt -s nocaseglob

for video in *.{mp4,mkv,avi}; do
    OUT=".tmp/${video%.*}-resized.mp4"
    ffmpeg -i "${video}" \
    -vf "scale=${RESOLUTION_X}:${RESOLUTION_Y}:force_original_aspect_ratio=decrease,pad=${RESOLUTION_X}:${RESOLUTION_Y}:-1:-1:color=black" \
    -c:v ${VIDEO_CODEC} -b:v ${VIDEO_BITRATE} -c:a ${AUDIO_CODEC} -b:a ${AUDIO_BITRATE} \
    "${OUT}"
    echo "file '${OUT}'" >> ./.list.txt
done

shopt -u nocaseglob
shopt -u nullglob

shuf ./.list.txt > ./.shuffled.txt

ffmpeg -f concat -safe 0 -i ./.shuffled.txt -c:v copy -c:a copy ./out/output.mp4
